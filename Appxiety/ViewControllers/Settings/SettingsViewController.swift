//
//  SettingsViewController.swift
//  Appxiety
//
//  Created by trying-things on 11/07/2018.
//  Copyright © 2018 Appxiety. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    override func viewWillAppear(_ animated: Bool) {
        // TODO: Look in database for helperEndTime and set Saved.online as true or false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if Saved.online == true {
            // Set Online as selected
            segmentedControl.selectedSegmentIndex = 1
        } else {
            // Set Offline as selected
            segmentedControl.selectedSegmentIndex = 0
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToInCallView(_:)), name: Notification.Name.StartCall, object: nil)
        
        if Saved.online == true {
            // Set Online as selected
            segmentedControl.selectedSegmentIndex = 1
        } else {
            // Set Offline as selected
            segmentedControl.selectedSegmentIndex = 0
        }
    }
    
    @IBAction func pressSegmentedControl(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            print("Pressed Offline")
            Saved.online = false
            FirebaseWrite.removeHelperFromDatabase()
        case 1:
            print("Pressed Online")
            performSegue(withIdentifier: "SettingsToTimeOnline", sender: self)
        default:
            print("SettingsViewController.pressSegmentedControl has no case for \(segmentedControl.selectedSegmentIndex)")
        }
    }
    
    @objc func moveToInCallView(_ notification: Notification) {
        
        self.performSegue(withIdentifier: "SettingsInCall", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
