//
//  TimeOnlineViewController.swift
//  Appxiety
//
//  Created by trying-things on 11/07/2018.
//  Copyright © 2018 Appxiety. All rights reserved.
//

import UIKit

class TimeOnlineViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        // Set the number of options in the picker
        return 4
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch row {
        case 0:
            return "5 Min"
        case 1:
            return "15 Min"
        case 2:
            return "1 Hr"
        case 3:
            return "24 Hrs"
        default:
            return "Unknown"
        }
    }
    
    
    @IBOutlet weak var timePicker: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToInCallView(_:)), name: Notification.Name.StartCall, object: nil)

        timePicker.delegate = self
        
    }
    
    @IBAction func pressSave(_ sender: Any) {
        // Find the selected time
        let selectedRow = timePicker.selectedRow(inComponent: 0)
        if let selectedTime = pickerView(timePicker, titleForRow: selectedRow, forComponent: 0) {
            // Get the time to go offline
            if let offlineTime = getOfflineTime(from: selectedTime) {
                print("Chosen to go offline at \(offlineTime)")
                // Save offline time in Saved.swift
                Saved.online = true
                Saved.goOffline = offlineTime
                FirebaseWrite.addToHelpersWaiting(endTime: offlineTime)
            } else {
                print("TimeOnlineViewController.pressSave.offlineTime = nil")
            }
        } else {
            print("TimeOnlineViewController.pressSave.selectedTime = nil")
        }
        
    }
    
    func getOfflineTime(from selectedTime: String) -> Date? {

        // Convert the title into minutes and work out the time to go offline
        switch selectedTime {
        case "5 Min":
            return timeFromNow(minutes: 5)
        case "15 Min":
            return timeFromNow(minutes: 15)
        case "1 Hr":
            return timeFromNow(minutes: 60)
        case "24 Hrs":
            return timeFromNow(minutes: 1440)
        default:
            print("HelperChooseViewController.pressStart().switch couldn't find a case for \(String(describing: selectedTime)) when converting title to minutes")
        }
        return nil
    }
    
    
    func timeFromNow(minutes: Int) -> Date? {
        
        var timeFromNow = Date()
        
        // I tried making it simpler with [minutes * 60] and got an error saying I couldn't use Int and Date together.
        // Switch handles it okay. I'm a bit confused, but this works!
        switch minutes {
        case 5:
            timeFromNow += 5 * 60
            return timeFromNow
        case 15:
            timeFromNow += 15 * 60
            return timeFromNow
        case 60:
            timeFromNow += 60 * 60
            return timeFromNow
        case 1440:
            timeFromNow += 1440 * 60
            return timeFromNow
        default:
            print("TimeOnlineViewController.getOfflineTime.minutes has no case for \(minutes)")
            return nil
        }
    }
    
    @objc func moveToInCallView(_ notification: Notification) {
        
        self.performSegue(withIdentifier: "TimeOnlineInCall", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
