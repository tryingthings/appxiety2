//
//  Saved.swift
//  Appxiety
//
//  Created by trying-things on 11/07/2018.
//  Copyright © 2018 Appxiety. All rights reserved.
//

import UIKit

class Saved: NSObject {

    // My Firebase anonymous ID
    static var myID: String?
    
    // My Apple Push Notification token
    static var myPushToken: String?
    
    // IDs for starting WebRTC connection
    static var anxiousID: String?
    
    static var helperID: String?
    
    // Online status
    static var online: Bool?
    
    // Time to go offline
    static var goOffline: Date?
    
    // My profile settings
    static var myName: String?
    static var aboutMe: String?
    static var aboutMyAnxiety: String?
    
    // Their profile settings
    static var theirName: String?
    static var aboutThem: String?
    static var aboutTheirAnxiety: String?
    
    // My language settings
    static var preferredLanguages: [String]?
}
