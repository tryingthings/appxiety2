//
//  MenuViewController.swift
//  Appxiety
//
//  Created by trying-things on 21/07/2018.
//  Copyright © 2018 Appxiety. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    
    @IBOutlet weak var name: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(moveToInCallView(_:)), name: Notification.Name.StartCall, object: nil)

    }
    
    @objc func moveToInCallView(_ notification: Notification) {
        
        // Make a manual segue from this view controller to InCallViewController
        self.performSegue(withIdentifier: "MenuToInCall", sender: self)
    }

   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
