//
//  MyProfileViewController.swift
//  Appxiety
//
//  Created by trying-things on 21/07/2018.
//  Copyright © 2018 Appxiety. All rights reserved.
//

import UIKit

class MyProfileViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {

    @IBOutlet weak var myNameTextField: UITextField!
    @IBOutlet weak var aboutMeTextField: UITextField!
    @IBOutlet weak var aboutMyAnxietyTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToInCallView(_:)), name: Notification.Name.StartCall, object: nil)

        myNameTextField.delegate = self
        aboutMeTextField.delegate = self
        aboutMyAnxietyTextField.delegate = self
        
        // replace with firebase
        myNameTextField.text = Saved.myName
        aboutMeTextField.text = Saved.aboutMe
        aboutMyAnxietyTextField.text = Saved.aboutMyAnxiety
    }
    
    @objc func moveToInCallView(_ notification: Notification) {
        
        // Make a manual segue from this view controller to InCallViewController
        self.performSegue(withIdentifier: "ProfileToInCall", sender: self)
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        
        Saved.myName = myNameTextField.text
        Saved.aboutMe = aboutMeTextField.text
        Saved.aboutMyAnxiety = aboutMyAnxietyTextField.text
        
        FirebaseWrite.saveProfile(myNameTextField.text, aboutMeTextField.text, aboutMyAnxietyTextField.text)
        
        print("MyProfileViewController.viewWillDisappear() saved: \n \(Saved.myName) \n \(Saved.aboutMe) \n \(Saved.aboutMyAnxiety)")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
