//
//  InCallViewController.swift
//  Appxiety
//
//  Created by trying-things on 14/07/2018.
//  Copyright © 2018 Appxiety. All rights reserved.
//

import UIKit

class InCallViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(closeCallView(_:)), name: Notification.Name.CloseCallView, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(segueToCallConnected(_:)), name: Notification.Name.WebRTC_Connected, object: nil)
        

        guard let myID = Saved.myID else {
            print("InCallViewController found myID is nil")
            return
        }
        if Saved.helperID == myID {
            NotificationCenter.default.post(Notification(name: .StartWebRTC))

        } else if Saved.helperID == nil {
            // Need a delay to allow FirebaseListen.getAnxiousID() to get results
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                NotificationCenter.default.post(Notification(name: .StartWebRTC))
            }
        } else {
            NotificationCenter.default.post(Notification(name: .StartWebRTC))
        }
    }
    
    @IBAction func pressHangUp(_ sender: Any) {
        if #available(iOS 10.0, *) {
            CallKitDelegate.sharedInstance.endCall()
        } else {
            iOS9Delegate.sharedInstance.endCall()
        }
    }
    
    @objc func closeCallView(_ notification: Notification) {
        self.performSegue(withIdentifier: "CloseCallView", sender: self)
        if #available(iOS 10.0, *) {
            CallKitDelegate.sharedInstance.endCall()
        } else {
            iOS9Delegate.sharedInstance.endCall()
        }
    }
    
    @objc func segueToCallConnected(_ notification: Notification) {
        self.performSegue(withIdentifier: "CallConnected", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
