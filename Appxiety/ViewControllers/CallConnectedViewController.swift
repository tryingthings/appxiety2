//
//  CallConnectedViewController.swift
//  Appxiety
//
//  Created by trying-things on 21/07/2018.
//  Copyright © 2018 Appxiety. All rights reserved.
//

import UIKit

class CallConnectedViewController: UIViewController {

    @IBOutlet weak var connectedToLabel: UILabel!
    @IBOutlet weak var profileLabel: UILabel!
    
    var profileDisplayed: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(closeCallView(_:)), name: Notification.Name.CloseCallView, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(receivedProfileName(_:)), name: Notification.Name.ReceivedProfileName, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(receivedProfileAbout(_:)), name: Notification.Name.ReceivedProfileAbout, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(receivedProfileAboutAnxiety(_:)), name: Notification.Name.ReceivedProfileAboutAnxiety, object: nil)
        
        print("CallConnectedViewController(): \n Saved.myID: \(Saved.myID.debugDescription) \n Saved.helperID: \(Saved.helperID.debugDescription) \n Saved.anxiousID \(Saved.anxiousID.debugDescription)")
       
        if let myID = Saved.myID {
            if let helperID = Saved.helperID {
                if let anxiousID = Saved.anxiousID {
                    if myID == helperID {
                        print("CallConnectedViewController() I am helper")
                        FirebaseListen.getTheirProfile(anxiousID)
                    } else if myID == anxiousID {
                        print("CallConnectedViewController() I am anxious")
                        FirebaseListen.getTheirProfile(helperID)
                    }
                }
            }
        }
    }
    
    @objc func receivedProfileName(_ notification: Notification) {
        if let theirName = Saved.theirName {
            print("CallConnectedViewController.receivedProfile() theirName: \(theirName)")
            connectedToLabel.text = "Connected to \(theirName)"
        }
    }
    
    @objc func receivedProfileAbout(_ notification: Notification) {
        if let aboutThem = Saved.aboutThem {
            print("CallConnectedViewController.receivedProfile() aboutThem: \(aboutThem)")
        }
    }
    
    @objc func receivedProfileAboutAnxiety(_ notification: Notification) {
        if let aboutMyAnxiety = Saved.aboutMyAnxiety {
            print("CallConnectedViewController.receivedProfile() aboutMyAnxiety: \(aboutMyAnxiety)")
        }
    }
    
    @IBAction func pressFire(_ sender: Any) {
        switch profileDisplayed {
        case nil:
            profileLabel.text = Saved.aboutThem
            profileDisplayed = "aboutThem"
        case "aboutThem":
            profileLabel.text = Saved.aboutMyAnxiety
            profileDisplayed = "aboutMyAnxiety"
        case "aboutMyAnxiety":
            profileLabel.text = ""
            profileDisplayed = nil
        default:
            print("CallConnectedViewController.pressFire() has no case for \(String(describing: profileDisplayed))")
        }

    }
    
    @IBAction func pressHangUp(_ sender: Any) {
        if #available(iOS 10.0, *) {
            CallKitDelegate.sharedInstance.endCall()
        } else {
            iOS9Delegate.sharedInstance.endCall()
        }
    }
    
    @objc func closeCallView(_ notification: Notification) {
        self.performSegue(withIdentifier: "CloseCallView", sender: self)
        if #available(iOS 10.0, *) {
            CallKitDelegate.sharedInstance.endCall()
        } else {
            iOS9Delegate.sharedInstance.endCall()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
