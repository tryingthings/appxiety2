//
//  ComingPanicViewController.swift
//  Appxiety
//
//  Created by trying-things on 11/07/2018.
//  Copyright © 2018 Appxiety. All rights reserved.
//

import UIKit

class ComingPanicViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Observe database for WebRTC helper then
        NotificationCenter.default.addObserver(self, selector: #selector(moveToInCallView(_:)), name: Notification.Name.StartCall, object: nil)

    }
    
    @objc func moveToInCallView(_ notification: Notification) {
        self.performSegue(withIdentifier: "ComingPanicInCall", sender: self)
        // Start webRTC here?
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
