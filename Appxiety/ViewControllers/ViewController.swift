//
//  ViewController.swift
//  Appxiety
//
//  Created by trying-things on 11/07/2018.
//  Copyright © 2018 Appxiety. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToInCallView(_:)), name: Notification.Name.StartCall, object: nil)
        
    }

    @IBAction func pressPanicComing(_ sender: Any) {
        FirebaseListen.anxiousChooseHelper()
    }

    
    @IBAction func pressPanicHaving(_ sender: Any) {
        FirebaseListen.anxiousChooseHelper()
    }
    
    @objc func moveToInCallView(_ notification: Notification) {
        
        self.performSegue(withIdentifier: "HomeScreenInCall", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

