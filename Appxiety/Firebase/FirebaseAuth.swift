//
//  FirebaseAuth.swift
//  Appxiety
//
//  Created by trying-things on 11/07/2018.
//  Copyright © 2018 Appxiety. All rights reserved.
//

import UIKit
import FirebaseAuth

class FirebaseAuth: NSObject {
    
    static func signInAnonymously() {
        // Automatically give an ID of random letters/numbers to the user
        Auth.auth().signInAnonymously() { (authResult, error) in
            if error == nil {
                if let user = authResult?.user {
                    let isAnonymous = user.isAnonymous  // true
                    let uid = user.uid // eg. EGnhg3jvumTbXBiraDv0Tq142KK2
                    print("User ID \(uid) is anonymous: \(isAnonymous)")
                    Saved.myID = uid
                    FirebaseListen.getMyProfile()
                }
            } else {
                print("Firebase Auth error: \(String(describing: error))")
            }
        }
    }
}
