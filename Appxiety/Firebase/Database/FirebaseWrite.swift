//
//  FirebaseWrite.swift
//  Appxiety
//
//  Created by trying-things on 14/07/2018.
//  Copyright © 2018 Appxiety. All rights reserved.
//

import UIKit

class FirebaseWrite: NSObject {
    
    static func saveProfile(_ myName:String?, _ aboutMe: String?, _ aboutMyAnxiety: String?){
        guard let myID = Saved.myID else {
            print("saveProfile() found myID is nil")
            return
        }
        if let myName = myName {
            FirebaseReference.profiles.child(myID).child("myName").setValue(myName)
        }
        if let aboutMe = aboutMe {
            FirebaseReference.profiles.child(myID).child("aboutMe").setValue(aboutMe)
        }
        if let aboutMyAnxiety = aboutMyAnxiety {
            FirebaseReference.profiles.child(myID).child("aboutMyAnxiety").setValue(aboutMyAnxiety)
        }
    }

    // Add ID when helper is waiting for anxious people to call
    static func addToHelpersWaiting(endTime: Date) {
        guard let myID = Saved.myID else {
            print("FirebaseWrite.addToHelpersWaiting() found myID is nil")
            return
        }
        guard let myPushToken = Saved.myPushToken else {
            print("FirebaseWrite.addToHelpersWaiting() found myPushToken is nil")
            return
        }
        print("Add helper to helpersWaiting")
        FirebaseReference.helpersWaiting.child(myID).setValue(myPushToken)
        FirebaseReference.helpersEndTime.child(myID).setValue("\(endTime)")
    }
    
    static func addToHelpersWaitingAfterHangup() {
        print("Firebase.Write.addToHeleprsWaitingAfterHangup")
        // TODO: Check helper's end time
        guard let myID = Saved.myID else {
            print("FirebaseWrite.addToHelpersWaitingAfterHangup found myID is nil")
            return
        }
        guard let helperID = Saved.helperID else {
            print("FirebaseWrite.addToHelpersWaitingAfterHangup found helperID is nil")
            return
        }
        guard let myPushToken = Saved.myPushToken else {
            print("FirebaseWrite.addToHelpersWaitingAfterHangup found myPushToken is nil")
            return
        }
        if myID == helperID {
            FirebaseReference.helpersWaiting.child(myID).setValue(myPushToken)
        } else {
            print("FirebaseWrite.addToHelpersWaitingAfterHangup() I'm not helper")
        }
    }
    
    // Remove ID when helper doesn't want to receive calls
    static func removeHelperFromDatabase() {
        guard let myID = Saved.myID else {
            print("FirebaseWrite.addToHelpersWaiting found myID is nil")
            return
        }
        FirebaseReference.helpersWaiting.child(myID).removeValue()
        FirebaseReference.helpersEndTime.child(myID).removeValue()
    }
    
    static func moveHelperToCallRequest(_ helperID: String) {
        guard let myID = Saved.myID else {
            print("FirebaseWrite.addToHelpersWaiting found myID is nil")
            return
        }
        print("Move helper to callRequest")
        FirebaseReference.helpersWaiting.child(helperID).removeValue()
        FirebaseReference.callRequest.child(helperID).setValue(myID)
    }
    
    static func removeCallRequest() {
        guard let helperID = Saved.helperID else {
            print("FirebaseWrite.removeCallRequest found helperID is nil")
            return
        }
        print("Remove call request")
        FirebaseReference.callRequest.child(helperID).removeValue()
    }
}
