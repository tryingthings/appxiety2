//
//  FirebaseReference.swift
//  Appxiety
//
//  Created by trying-things on 14/07/2018.
//  Copyright © 2018 Appxiety. All rights reserved.
//

import UIKit
import FirebaseDatabase

class FirebaseReference: NSObject {
 
    // Shortcuts for locations in the database
    static let helpersWaiting = Database.database().reference().child("helpers")
    static let helpersEndTime = Database.database().reference().child("helpersEndTime")
    static let callRequest = Database.database().reference().child("callRequest")
    static let profiles = Database.database().reference().child("profiles")
}
