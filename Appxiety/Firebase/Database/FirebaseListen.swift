//
//  FirebaseListen.swift
//  Appxiety
//
//  Created by trying-things on 14/07/2018.
//  Copyright © 2018 Appxiety. All rights reserved.
//

import UIKit

class FirebaseListen: NSObject {
    
    static func getMyProfile() {
        if let myID = Saved.myID {
            FirebaseReference.profiles.child(myID).child("myName").observe(.value) { (snapshot) in
                if snapshot.exists() {
                    print("FirebaseListen.getMyProfile() found myName snapshot")
                    print(snapshot)
                    Saved.myName = snapshot.value as? String
                } else {
                    print("FirebaseListen.getMyProfile() couldn't find myName snapshot")
                }
            }
            FirebaseReference.profiles.child(myID).child("aboutMe").observe(.value) { (snapshot) in
                if snapshot.exists() {
                    print("FirebaseListen.getMyProfile() found aboutMe snapshot")
                    print(snapshot)
                    Saved.aboutMe = snapshot.value as? String
                } else {
                    print("FirebaseListen.getMyProfile() couldn't find aboutMe snapshot")
                }
            }
            FirebaseReference.profiles.child(myID).child("aboutMyAnxiety").observe(.value) { (snapshot) in
                if snapshot.exists() {
                    print("FirebaseListen.getMyProfile() found aboutMyAnxiety snapshot")
                    print(snapshot)
                    Saved.aboutMyAnxiety = snapshot.value as? String
                } else {
                    print("FirebaseListen.getMyProfile() couldn't find aboutMyAnxiety snapshot")
                }
            }

        } else {
            print("FirebaseListen.getMyProfile() couldn't find myID")
        }
    }
    
    
    static func getTheirProfile(_ userID: String) {

        FirebaseReference.profiles.child(userID).child("myName").observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                print("FirebaseListen.getProfile() found myName snapshot")
                print(snapshot)
                Saved.theirName = snapshot.value as? String
                NotificationCenter.default.post(name: NSNotification.Name.ReceivedProfileName, object: nil)
            } else {
                print("FirebaseListen.getProfile() can't find myName snapshot")
            }
        }

        FirebaseReference.profiles.child(userID).child("aboutMe").observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                print("FirebaseListen.getProfile() found aboutMe snapshot")
                print(snapshot)
                Saved.aboutThem = snapshot.value as? String
                 NotificationCenter.default.post(name: NSNotification.Name.ReceivedProfileAbout, object: nil)
            } else {
                print("FirebaseListen.getProfile() can't find aboutMe snapshot")
            }
        }

        FirebaseReference.profiles.child(userID).child("aboutMyAnxiety").observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                print("FirebaseListen.getProfile() found aboutMyAnxiety snapshot")
                print(snapshot)
                Saved.aboutTheirAnxiety = snapshot.value as? String
                NotificationCenter.default.post(name: NSNotification.Name.ReceivedProfileAboutAnxiety, object: nil)
            } else {
                print("FirebaseListen.getProfile() can't find aboutMyAnxiety snapshot")
            }
        }
    
    }


    static func anxiousChooseHelper() {
        print("Listen for helpersWaiting")
        FirebaseReference.helpersWaiting.observe(.value) { (snapshot) in
            if snapshot.exists() {
                
                print(snapshot.debugDescription)
                
                FirebaseSnapshot.chooseHelper(snapshot)
                
            } else {
                print("FirebaseListen.anxiousChooseHelper() found no helpers waiting")
            }
        }
    }
    
    static func stopAnxiousChoosingHelper() {
        print("Stop listening for helpersWaiting")
        FirebaseReference.helpersWaiting.removeAllObservers()
    }
    
    static func getAnxiousID() {
        print("Helper listening for anxious ID")
        
        FirebaseReference.callRequest.observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                
                print(snapshot.debugDescription)
                
                if let anxiousID = FirebaseSnapshot.getAnxiousID(snapshot) {
                    print("FirebaseListen.getAnxiousID() anxiousID: \(anxiousID)")
                    
                    Saved.anxiousID = anxiousID
                    Saved.helperID = Saved.myID!
                    FirebaseWebRTC.shared.sender = Saved.myID!
                    FirebaseWebRTC.shared.receiver = anxiousID
                }
            }

        }
    }
    
    
}
