//
//  FirebaseSnapshot.swift
//  Appxiety
//
//  Created by trying-things on 14/07/2018.
//  Copyright © 2018 Appxiety. All rights reserved.
//

import UIKit
import FirebaseDatabase

class FirebaseSnapshot: NSObject {
    
    
    static func helperIsOnline(_ helperID: String, offlineTime: String) -> Bool {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZZZ"
        print("Checking \(helperID) : \(offlineTime)")
        if let offlineDate = dateFormatter.date(from: offlineTime)  {
            print("FirebaseSnapshot.helperIsOnline() Success converting to date: \(offlineTime) : \(helperID)")
            let now = Date()
            print("FirebaseSnapshot.helperIsOnline() \(now)")
            print("FirebaseSnapshot.helperIsOnline() \(offlineDate) for \(helperID)")
            
            
            if now <= offlineDate {
                print("FirebaseSnapshot.helperIsOnline() now is before offline date: \(helperID)")
                return true
            } else if now >= offlineDate {
                print("FirebaseSnapshot.helperIsOnline() now is after offline date: \(helperID)")
                return false
            } else {
                print("FirebaseSnapshot.helperIsOnline() I don't know: now: \(now), offline: \(offlineDate)")
            }
        } else {
            print("FirebaseSnapshot.helperIsOnline() couldn't convert string to date for \(helperID)")
        }
        return false
    }

    static func chooseHelper(_ snapshot: DataSnapshot) {
        
        let dispatchGroup = DispatchGroup()
        
        if let snapshotDict = (snapshot.value as? [String : String]) {
            print("FirebaseSnapshot.chooseHelper() convert snapshot to dictionary: \(snapshotDict)")
            var helperIDs = [String]()
            for (helperID, _) in snapshotDict {
                if let myID = Saved.myID {
                    if helperID == myID {
                        // Don't add my ID to list of helpers - I don't want to choose myself.
                    } else {
                        dispatchGroup.enter()
                        FirebaseReference.helpersEndTime.child(helperID).observeSingleEvent(of: .value) { (snapshot) in
                            if snapshot.exists() {
                                if let time = snapshot.value as? String {
                                    if helperIsOnline(helperID, offlineTime: time) {
                                        print("FirebaseSnapshot.chooseHelper() Add \(helperID) to dictionary")
                                        helperIDs.append(helperID)
                                        dispatchGroup.leave()
                                    }
                                }
                            }
                        }
                    }
                } else {
                    print("FirebaseSnapshot.chooseHelper() found myID is nil")
                }
            }
            
            dispatchGroup.notify(queue: .main) {
                print("FirebaseSnapshot.chooseHelper() helperIDs: \(helperIDs)")
                if helperIDs.count == 1 {
                    let helper = helperIDs[0]
                    if let pushToken = snapshotDict[helper] {
                        print("FirebaseSnapshot.chooseHelper() helper: \(helper), pushToken: \(pushToken)")
                        startCall(helper, pushToken)
                    }
                } else if helperIDs.count > 1 {
                    let randomNumber = Int.random(in: 0 ..< helperIDs.count)
                    let helper = helperIDs[randomNumber]
                    if let pushToken = snapshotDict[helper] {
                        print("FirebaseSnapshot.chooseHelper() helper: \(helper), pushToken: \(pushToken)")
                        startCall(helper, pushToken)
                    }
                } else {
                    print("FirebaseSnapshot.chooseHelper() found no other helpers online ")
                }
            }
            
            func startCall(_ helper: String?, _ pushToken: String?) {
                guard let helperID = helper else {
                    print("FirebaseListen.anxiousChooseHelper() found no helperID")
                    return
                }
                guard let helperPushToken = pushToken else {
                    print("FirebaseListen.anxiousChooseHelper() found no helperPushToken")
                    return
                }
                
                
                NotificationCenter.default.post(name:
                    Notification.Name.HelperChosen, object: nil)
                print("FirebaseListen.anxiousChooseHelper() helperID: \(helperID), helperPushToken: \(helperPushToken)")
                Saved.helperID = helperID
                Saved.anxiousID = Saved.myID!
                FirebaseWebRTC.shared.sender = Saved.myID!
                FirebaseWebRTC.shared.receiver = helperID
                FirebaseWrite.moveHelperToCallRequest(helperID)
                if #available(iOS 10.0, *) {
                    let callkitDelegate = CallKitDelegate.sharedInstance
                    sendNotification(to: helperPushToken)
                    callkitDelegate.startCall()
                    NotificationCenter.default.post(name:
                        Notification.Name.StartCall, object: nil)
                } else {
                    sendNotification(to: helperPushToken)
                    NotificationCenter.default.post(name:
                        Notification.Name.StartCall, object: nil)
                }
                
            }
        }
    }
    
    static func sendNotification(to token:String) {
        print("Sending notification to \(token)")
        let php = URL(string: "https://mysterious-escarpment-87606.herokuapp.com/index.php?token=\(token)")
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: php!)
        
        //        let task = session.dataTask(with: php) { (<#Data?#>, <#URLResponse?#>, <#Error?#>) in
        //            <#code#>
        //        }
        task.resume()
    }
    
    static func getAnxiousID(_ snapshot: DataSnapshot) -> String? {
        guard let myID = Saved.myID else {
            print("FirebaseSnapshot.getAnxiousID() found myID is nil")
            return nil
        }
        if let snapshotDict = (snapshot.value as? [String : String]) {
            print("FirebaseSnapshot.getAnxiousID() convert snapshot to dictionary: \(snapshotDict)")
            for (helperID, anxiousID) in snapshotDict {
                if helperID == myID {
                    return anxiousID
                }
            }
        }
        return nil
    }
}
