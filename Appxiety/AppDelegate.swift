//
//  AppDelegate.swift
//  Appxiety
//
//  Created by trying-things on 11/07/2018.
//  Copyright © 2018 Appxiety. All rights reserved.
//

import UIKit
import Firebase
import WebRTC

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var webRTC: WebRTC?
    var isCallOngoing = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        FirebaseAuth.signInAnonymously()
        
        application.registerUserNotificationSettings(
            UIUserNotificationSettings(
                types:[.alert, .sound, .badge],
                categories: nil
            )
        )
                
        if self.isCallOngoing {
            self.isCallOngoing = false
            self.loadCallerController()
        }
        PushKitDelegate.sharedInstance.registerPushKit()
        
        Saved.preferredLanguages = Locale.preferredLanguages
        for language in Saved.preferredLanguages! {
            let components = NSLocale.components(fromLocaleIdentifier: language)
            let locale = NSLocale.init(localeIdentifier: language)
            let key = NSLocale.Key.languageCode
            print("lang key: \(key)")
            let displayName = locale.displayName(forKey: key, value: language)
            
            
            print("Preferred language components: \(components), displayName: \(displayName)")
            
        }
        print("Preferred languages: \(String(describing: Saved.preferredLanguages))")
    

        // Listen for notifications to start/close a WebRTC instance
         NotificationCenter.default.addObserver(self, selector: #selector(self.helperChosen(_:)), name: .HelperChosen, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.startWebRTC(_:)), name: .StartWebRTC, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.closeWebRTC(_:)), name: .CloseWebRTC, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.webRTC_Offer(_:)), name: .WebRTC_Offer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.webRTC_Answer(_:)), name: .WebRTC_Answer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.webRTC_Candidate(_:)), name: .WebRTC_Candidate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.webRTC_Close(_:)), name: .WebRTC_Close, object: nil)
        
        return true
        
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        // iOS 9: Received notification while app is in foreground
        print("i0S 9 received notification in foreground")
        // Move to InCallViewController
        NotificationCenter.default.post(name: Notification.Name.StartCall, object: nil)
    }
    
    @objc func helperChosen(_ notification: Notification) {
        print("AppDelegate.helperChosen()")
        FirebaseListen.stopAnxiousChoosingHelper()
    }
    
    @objc func startWebRTC(_ notification: Notification) {
        print("AppDelegate.startWebRTC(): Starting WebRTC instance")
        
        
        if webRTC == nil {
            print("AppDelegate.startWebRTC(): New WebRTC instance started")
            webRTC = WebRTC()
            if let webRTC = webRTC {
                webRTC.peerConnectionFactory = RTCPeerConnectionFactory()
                webRTC.startAudio()
                let firebase = FirebaseWebRTC.shared
                firebase.setup()
                if webRTC.peerConnection == nil {
                    print("make offer")
                    webRTC.makeOffer()
                } else {
                    print("peer already exists")
                }
            }
        } else {
            print("startWebRTC(): WebRTC already exists")
        }
    }
    
    @objc func closeWebRTC(_ notification: Notification) {
        print("AppDelegate.closeWebRTC(): Closing WebRTC instance")
        
        if webRTC == nil {
            print("AppDelegate.closeWebRTC(): WebRTC is already nil")
        } else {
            if let webRTC = webRTC {
                print("AppDelegate.closeWebRTC(): called WebRTC.close()")
                webRTC.close()
            }
            print("AppDelegate.closeWebRTC(): WebRTC instance is now nil")
            webRTC = nil
        }
    }
    
    @objc func webRTC_Offer(_ notification: Notification) {
        print("AppDelegate.webRTC_Offer() received offer")
        let offer = notification.object as! RTCSessionDescription
        if let webRTC = webRTC {
            print("AppDelegate.webRTC_Offer() sent offer to WebRTC.setOffer()")
            webRTC.setOffer(offer)
        }
    }
    
    @objc func webRTC_Answer(_ notification: Notification) {
        print("AppDelegate.webRTC_Answer() received answer")
        let answer = notification.object as! RTCSessionDescription
        if let webRTC = webRTC {
            print("AppDelegate.webRTC_Answer() sent answer to WebRTC.setAnswer()")
            webRTC.setAnswer(answer)
        }
    }
    
    @objc func webRTC_Candidate(_ notification: Notification) {
        print("AppDelegate.webRTC_Candidate() received candidate")
        let candidate = notification.object as! RTCIceCandidate
        if let webRTC = webRTC {
            print("AppDelegate.webRTC_andidateC() sent candidate to WebRTC.setCandidate()")
            webRTC.addIceCandidate(candidate)
        }
    }
    
    @objc func webRTC_Close(_ notification: Notification) {
        print("AppDelegate.webRTC_Close() received close notification")
        if let webRTC = webRTC {
            webRTC.close()
        }
    }

    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        if self.isCallOngoing {
            self.isCallOngoing = false
            self.loadCallerController()
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        if self.isCallOngoing {
            self.isCallOngoing = false
            self.loadCallerController()
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension AppDelegate {
    
    func setRootView(controller: UIViewController) {
        self.window?.rootViewController = controller
        self.window?.makeKeyAndVisible()
    }
    
    func loadCallerController() {
        let controller = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "InCall") as! InCallViewController
        self.setRootView(controller: controller)
    }
}

extension NSNotification.Name {
    
    static let ReceivedProfileName = Notification.Name("ReceivedProfileName")
    static let ReceivedProfileAbout = Notification.Name("ReceivedProfileAbout")
    static let ReceivedProfileAboutAnxiety = Notification.Name("ReceivedProfileAboutAnxiety")
    static let HelperChosen = Notification.Name("HelperChosen")
    static let StartCall = Notification.Name("StartCall")
    static let StartWebRTC = Notification.Name("StartWebRTC")
    static let CloseWebRTC = Notification.Name("CloseWebRTC")
    static let CloseCallView = Notification.Name("CloseCallView")
    static let WebRTC_Offer = Notification.Name("WebRTC_Offer")
    static let WebRTC_Answer = Notification.Name("WebRTC_Answer")
    static let WebRTC_Candidate = Notification.Name("WebRTC_Candidate")
    static let WebRTC_Close = Notification.Name("WebRTC_Close")
    static let WebRTC_Connected = Notification.Name("WebRTC_Connected")
}
