//
//  PushKitDelegate.swift
//  Appxiety
//
//  Created by trying-things on 14/07/2018.
//  Copyright © 2018 Appxiety. All rights reserved.
//

import Foundation
import UIKit
import PushKit
import UserNotifications
import CallKit

class PushKitDelegate: NSObject {
    
    static let sharedInstance = PushKitDelegate()
    
    func registerPushKit() {
        if #available(iOS 8.0, *) {
            print("Registering PushKit")
            let voipRegistry: PKPushRegistry = PKPushRegistry(queue: DispatchQueue.main)
            voipRegistry.delegate = self
            voipRegistry.desiredPushTypes = [PKPushType.voIP]
        }
    }
}

extension PushKitDelegate: PKPushRegistryDelegate {
    
    @available(iOS 8.0, *)
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        let token = pushCredentials.token.reduce("", {$0 + String(format: "%02X", $1)})
        //        print(pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined())
        print("PushKit token: \(token)")
        Saved.myPushToken = token
    }
    
    @available(iOS, introduced: 8.0, deprecated: 11.0)
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType) {
        let delegate = UIApplication.shared.delegate as? AppDelegate
        delegate?.isCallOngoing = true
        if #available(iOS 10.0, *) {
            CallKitDelegate.sharedInstance.reportIncomingCall {
            }
        } else {
            print("Received push notification: \(payload)")
            iOS9Delegate.sharedInstance.reportIncomingCall()
        }
    }
    
    @available(iOS 11.0, *)
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        
        print(payload.dictionaryPayload)
        let delegate = UIApplication.shared.delegate as? AppDelegate
        delegate?.isCallOngoing = true
        CallKitDelegate.sharedInstance.reportIncomingCall {
            completion()
        }
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        print("Invalidated for type: \(type)")
    }
}
