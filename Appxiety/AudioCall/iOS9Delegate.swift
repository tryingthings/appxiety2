//
//  iOS9Delegate.swift
//  Appxiety
//
//  Created by trying-things on 16/07/2018.
//  Copyright © 2018 Appxiety. All rights reserved.
//

import UIKit

class iOS9Delegate: NSObject {

    static let sharedInstance = iOS9Delegate()

    func reportIncomingCall() {
        let localNotification = UILocalNotification()
        localNotification.fireDate = Date()
        localNotification.alertBody = "You're needed!"
        localNotification.timeZone = NSTimeZone.default
        localNotification.alertAction = "Answer"
        UIApplication.shared.scheduleLocalNotification(localNotification)
        
        FirebaseListen.getAnxiousID()
    }
    
    func endCall() {
        let delegate = UIApplication.shared.delegate as? AppDelegate
        delegate?.isCallOngoing = false
        NotificationCenter.default.post(Notification(name: .CloseWebRTC))
    }
}
