//
//  CallKitDelegate.swift
//  Appxiety
//
//  Created by trying-things on 14/07/2018.
//  Copyright © 2018 Appxiety. All rights reserved.
//

import Foundation
import UIKit
import CallKit

@available(iOS 10.0, *)
class CallKitDelegate: NSObject {
    
    static let sharedInstance = CallKitDelegate()
    
    fileprivate var uuid: UUID?
    fileprivate var provider: CXProvider?
    
    func reportIncomingCall(completionHandler: @escaping ()->Void) {
        let config = CXProviderConfiguration(localizedName: "Appxiety")
//        config.iconTemplateImageData = UIImage(named: "telephone")!.pngData()
        config.ringtoneSound = "incoming.wav"
        
        if #available(iOS 11.0, *) {
            config.includesCallsInRecents = false
        }
        config.supportsVideo = false
        self.provider = CXProvider(configuration: config)
        self.provider?.setDelegate(self, queue: nil)
        let update = CXCallUpdate()
        update.remoteHandle = CXHandle(type: .generic, value: "Appxiety")
        update.hasVideo = false
        self.uuid = UUID()
        self.provider?.reportNewIncomingCall(with: self.uuid!, update: update) { (error) in
            completionHandler()
        }
    }
    
    func startCall() {
        print("Calling...")
        let provider = CXProvider(configuration: CXProviderConfiguration(localizedName: "Appxiety"))
        provider.setDelegate(self, queue: nil)
        let controller = CXCallController()
        self.uuid = UUID()
        let transaction = CXTransaction(action: CXStartCallAction(call: self.uuid!, handle: CXHandle(type: .generic, value: "Appxiety")))
        controller.request(transaction, completion: { error in })
        
        DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 6) {
            provider.reportOutgoingCall(with: controller.callObserver.calls[0].uuid, connectedAt: nil)
        }
    }
    
    @objc func endCall() {
        print("End call")
        NotificationCenter.default.post(Notification(name: .CloseWebRTC))

        guard let uuid = uuid else {
            print("uuid is nil")
            return
        }
        let endCallAction = CXEndCallAction(call: uuid)
        let transaction = CXTransaction(action: endCallAction)
        CXCallController().request(transaction) { (error) in
            if let _ = error {
                self.provider?.reportCall(with: uuid, endedAt: Date(), reason: .remoteEnded)
                return
            }
        }
    }
    
}

@available(iOS 10.0, *)
extension CallKitDelegate: CXProviderDelegate {
    @available(iOS 10.0, *)
    func providerDidReset(_ provider: CXProvider) {
        print("CallKit reset")
    }
    
    @available(iOS 10.0, *)
    func providerDidBegin(_ provider: CXProvider) {
        print("CallKit begin")
        
        guard let myID = Saved.myID else {
            print("InCallViewController found myID is nil")
            return
        }
        if Saved.helperID == myID {
            FirebaseListen.getAnxiousID()
        } else {
            // do nothing
        }
    }
    
    @available(iOS 10.0, *)
    func provider(_ provider: CXProvider, perform action: CXStartCallAction) {
        print("CallKit start call")
        action.fulfill()
    }
    
    @available(iOS 10.0, *)
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        print("CallKit answer call")
        
        print("Start call with me: \(String(describing: Saved.myID)) and anxious: \(String(describing: Saved.anxiousID))")
        
        action.fulfill()
    }
    
    @available(iOS 10.0, *)
    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        print("CallKit end call")
        let delegate = UIApplication.shared.delegate as? AppDelegate
        delegate?.isCallOngoing = false
        NotificationCenter.default.post(name: .WebRTC_Close, object: nil)
        action.fulfill()
    }
    
    @available(iOS 10.0, *)
    func provider(_ provider: CXProvider, timedOutPerforming action: CXAction) {
        print("CallKit timed out")
    }
}
